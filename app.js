require('dotenv').config();
const Koa =  require('koa');
const bodyParser = require('koa-bodyparser');
const serve = require('koa-static')
const config = require('./config');
const app = new Koa();
const PORT = config.port;
const auth = require('./lib/auth');
const router = require('./routes');
const cors = require('koa2-cors');

app.use(cors());
app.use(bodyParser({
  extendTypes: {
    json: ['application/json'] // will parse application/x-javascript type body as a JSON string
  }
}));

/*
app.use(async (ctx, next) => {
  console.log("ctx.request.method =>",ctx.request);
    if(ctx.request.method ==='POST'){
        let token = ctx.request.headers.token || ctx.request.body.token;
        let status = false, message = null;
        if (token) {
            await auth.varifyToken(token)
                .then(result => {
                    if(result.status){
                        status = true;
                    }
                    else if(result.message) {
                        message = result.message;
                    }
                    else {
                        console.log('3')
                        message = result;
                    }
                }).catch(err => {
                    message = err;
                });
        }
        else {
            message = 'Token is not available in request'
        }
        if (status) {
            await next();
        }
        else {
            ctx.body = {
                status: status,
                message: message
            };
        }
    }
    else {
        await next();
    }

}); */
app.use(router.routes());


const server = app.listen(PORT, () => {
    console.log(`Server listening on port: ${PORT}`);
});

module.exports = server;
