var authAPI = require('./../config').authAPI;
var request = require('request-promise');

exports.varifyToken = (token) => {
    var options = {
        method: 'POST',
        uri: authAPI + '/validate',
        form: {
            token: token
        },
        headers: {
            /* 'content-type': 'application/x-www-form-urlencoded' */ // Is set automatically
        }
    };
    return request(options).then(body=>{
        let result = JSON.parse(body);
        return result
    }).catch(err =>{
        return err
    })
};

exports.userInfo = (token) => {
    var options = {
        method: 'POST',
        uri: authAPI + '/info',
        form: {
            token: token
        },
        headers: {
            /* 'content-type': 'application/x-www-form-urlencoded' */ // Is set automatically
        }
    };
    return request(options).then(body=>{
        let result = JSON.parse(body);
        return result
    }).catch(err =>{
        return err
    })
};
