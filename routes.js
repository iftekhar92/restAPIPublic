const Router = require('koa-router');
const router = new Router();
const controllers = require('./controllers');

router.get('/', async (ctx) => {
  console.log("test");
    ctx.body = {
        status: 'success',
        message: `This is messaging API. You can do many tasks on ejabberd server with this. Check ${process.env.HOST}:${process.env.PORT}/docs for more info.`
    };
});
router.post('/register', controllers.user.register);

module.exports = router;
