module.exports = {
    port: process.env.PORT,
    ejabberd : {
        host: process.env.EJABBERD_REST_API_ADDR,
        key: process.env.SECRET_KEY
    },
    authAPI: process.env.AUTH_API_ADDR,
    nodemailer : {
      host: process.env.APP_EMAIL_HOST,
      port: process.env.APP_EMAIL_PORT,
      secure: process.env.APP_EMAIL_SECURE,
      auth:{
        user: process.env.APP_EMAIL_USER,
        pass: process.env.APP_EMAIL_PASS
      },
      proxy: process.env.APP_EMAIL_PROXY
    },
    aws: {
      accessKeyId: process.env.AWS_ACCESS_KEY_ID,
      secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
      endpoint: process.env.AWS_END_POINT
    },
    elastic: {
      API: process.env.ELASTIC_SEARCH_HOST
    }
}
